package fr.ufds.cc;

import fr.ufds.cc.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

//import java.util.stream.Stream;
public class TestAffectation {
	GestionTER g1;
	GestionTER g2;
	
	Etudiant e1;
	Etudiant e2;
	Etudiant e3;
	Etudiant e4;
	Etudiant e5;
	
	Enseignant en1;
	Enseignant en2;
	
	Groupe gr1;
	Groupe gr2;
	Groupe gr3;
	Groupe gr4;
	
	Sujet s1;
	Sujet s2;
	Sujet s3;
	Sujet s4;
	Sujet s5;
	Sujet s6;
	
	@BeforeEach
	public void setUp() {
//		Creation des enseignants
		en1 = new Enseignant("prof1", "nom prof 1");
		en2 = new Enseignant("prof2", "nom prof 2");
		
//		Creer les sujets
		s1 = new Sujet("sujet 1", "Ceci est le résumé du sujet 1",en1);
		s2 = new Sujet("sujet 2", "Ceci est le résumé du sujet 2",en1);
		s3 = new Sujet("sujet 3", "Ceci est le résumé du sujet 3",en1);
		s4 = new Sujet("sujet 4", "Ceci est le résumé du sujet 4",en2);
		s5 = new Sujet("sujet 5", "Ceci est le résumé du sujet 5",en2);
		s6 = new Sujet("sujet 6", "Ceci est le résumé du sujet 6",en2);
		
//		creation de 2 groupes tests
//		1) faire une liste d etudiant a ajouter dans un groupe
		ArrayList<Etudiant> li1 = new ArrayList<Etudiant>(5);
		li1.add(e1);
		li1.add(e2);
		li1.add(e3);
		li1.add(e4);
		li1.add(e5);
		ArrayList<Etudiant> li2 = new ArrayList<Etudiant>(5);
		li2.add(e1);
		li2.add(e2);
		li2.add(e3);
		ArrayList<Etudiant> li3 = new ArrayList<Etudiant>(5);
		li3.add(e3);
		li3.add(e4);
		li3.add(e5);
		ArrayList<Etudiant> li4 = new ArrayList<Etudiant>(5);
		li4.add(e1);
		li4.add(e2);
		li4.add(e5);
//		2) ajouter les etudiants dans le groupe
		gr1 = new Groupe(e1,li1);
		gr2 = new Groupe(e2,li1);
		gr3 = new Groupe(e3,li1);
		gr4 = new Groupe(e5,li1);
		
//		3) Creer la liste des sujets
		ArrayList<Sujet> liS = new ArrayList<Sujet>(6);
		liS.add(s1);
		liS.add(s2);
		liS.add(s3);
		liS.add(s4);
		liS.add(s5);
		liS.add(s6);
//		Creation des listes des voeux du groupe
		ArrayList<Sujet> liS1 = new ArrayList<Sujet>(5);
		ArrayList<Sujet> liS2 = new ArrayList<Sujet>(5);
		
		liS1.add(s1);
		liS1.add(s2);
		liS1.add(s3);
		liS1.add(s4);
		liS1.add(s5);

		liS2.add(s1);
		liS2.add(s2);
		liS2.add(s3);
		liS2.add(s4);
		liS2.add(s6);
		
//		Ajout de la liste de voeux aux groupes
		gr1.setVoeux(liS1);
		gr2.setVoeux(liS1);
		gr3.setVoeux(liS2);
		gr4.setVoeux(liS2);
		
//		creation de la liste des groupes
		ArrayList<Groupe> liG = new ArrayList<Groupe>(4);
		liG.add(gr1);
		liG.add(gr2);
		liG.add(gr3);
		liG.add(gr4);
		
		g1 = new GestionTER(5,6);
		g2 = new GestionTER(5,6,liG, liS);
	}
	
	@Test
	public void testAffectation() {
		gr1.setSujetAffecte(s1);
		assertEquals(true, s1.getAffecte());
		gr2.setSujetAffecte(s2);
		assertEquals(true,s2.getAffecte());
		assertEquals(false,s3.getAffecte());
		assertEquals(false, s4.getAffecte());
	}

}
