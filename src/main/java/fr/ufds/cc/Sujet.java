package fr.ufds.cc;

import java.util.concurrent.atomic.AtomicInteger;

public class Sujet {
	private static final AtomicInteger ID_FACTORY=new AtomicInteger();
	private final int id;
	private String titre;
	private String resume;
	private Enseignant encadrant;
	private boolean affecte=false;

	public Sujet() {
		this.id=ID_FACTORY.getAndIncrement();
		this.titre="titre";
		this.resume="resume";
	}
	public Sujet(String titre, String resume, Enseignant encadrant) {
		this.id=ID_FACTORY.getAndIncrement();
		this.titre=titre;
		this.resume=resume;
		this.encadrant=encadrant;
	}
	
	public boolean getAffecte() {
		return affecte;
	}
	public void setAffecte(boolean affecte) {
		this.affecte = affecte;
	}

	public int getId() {
		return id;
	}
	public Enseignant getEncadrant() {
		return encadrant;
	}
	public void setEncadrant(Enseignant encadrant) {
		this.encadrant = encadrant;
	}
	public String getTitre() {
		return titre;
	}
	public void setSujet(String titre) {
		this.titre = titre;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}

}
